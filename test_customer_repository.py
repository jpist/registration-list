import uuid
from event_store.infrastructure.reflection.reflection import obj_name
from faker import Factory
from event import *
from customer import *
from unittest import TestCase
import unittest
from test_setup import real_event_store, in_memory_event_store
    
class CustomerRepositoryIntegrationTest(TestCase):
    def setUp(self):
        self.repository = CustomerRepository(in_memory_event_store())
        self.fake = Factory.create()
        self.customer_id = 'aaaa-54b0-4e1d-9650-93b229464b9a'
        self.setup_fixtures()
        
    def setup_fixtures(self):
        customer_id = self.customer_id
        customer = None
        try:
            customer = self.repository.load(customer_id)
            print("Customer found: %s" % customer.customer_id)
        except ValueError:
            # Customer not found - so create him
            print("Customer not found... creating!")
            self.repository.create(Customer(customer_id, 'Joe Doe'))
            customer = self.repository.load(customer_id)

        print("Customer retrieved: %s" % customer.name)

    def test_change_customer_name(self):
        cid = self.customer_id
        customer = self.repository.load(cid)
        new_name = self.fake.name()
        customer.change_name(new_name)
        self.repository.save(customer)

        # Reconstitute the object
        cust = self.repository.load(cid)
        self.assertEqual(new_name, cust.name)

    def test_delete_customer(self):
        cid = self.customer_id
        customer = self.repository.load(cid)
        customer.unregister('Bad Customer!')
        self.repository.save(customer)

        # Try to retrieve it again and modify it
        c = self.repository.load(cid)
        self.assertRaises(NonRegisteredCustomerException, c.change_name, ["Ill Bloom"])

        
        



if __name__ == '__main__':
    unittest.main()
