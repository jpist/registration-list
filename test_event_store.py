import uuid

from event_store.infrastructure.persistence.pickle_serializer import PickleSerializer
from event_store.infrastructure.persistence.sql_event_store import SqlEventStore
from event_store.infrastructure.reflection.reflection import obj_name
from faker import Factory
from commands import RegisterCustomerCommand, ChangeCustomerNameCommand
from event import CustomerRegisteredEvent


customer_id = uuid.uuid4()
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

default_database_url = 'postgresql://postgres:test@localhost:5432/event_store'

engine = create_engine(default_database_url)
Session = sessionmaker(bind=engine, autocommit=False)


# Begin transaction
session = Session()

# Transaction is apparently started automatically...
serializer = PickleSerializer()
ses = SqlEventStore(session, serializer)

#aggregate_id = uuid.UUID('607c24f2-cd0f-4877-91fb-4c1d91347ccd')

registration_list = RegistrationList(ses)

fake = Factory.create()
# Register a new customer
registration_list.receive_command(RegisterCustomerCommand(customer_id, fake.name()))

registration_list.shout()

registration_list.receive_command(ChangeCustomerName(customer_id, fake.name()))

registration_list.shout()

# Query for aggregate
#try:
#    aggregate = session.query(Aggregate).get(registration_list.persistence_id)
#    # CONCURRENCY PROBLEM CAN HAPPEN HERE <----> We can put this in a transaction.
#    ses.save_changes(aggregate_id=aggregate.aggregate_id,
#                     aggregate_type=aggregate_type,
#                     originating_version=aggregate.version,
#                     events=[)
#except ConcurrencyProblem:
#    # Try again
#    session.rollback()


#session.commit()

# Register another customer



print("Now getting events!")
evs = ses.get_events_for(registration_list.persistence_id)
print("From version 4: %s" % len(evs))
alle = ses.get_events_for(registration_list.persistence_id)
print("All evs: %s" % len(alle))
print("Got %s events" % len(evs))
for event in evs:
    print("Found event!: type=%s, name=%s" % (obj_name(event), event.name))
    # Will try to deserialize events ???
print("Done!")


# Rebuild registration list from the events
registration_list = RegistrationList(ses)
for event in evs:
    registration_list.receive_recover(event)

# Print currently registered customers:
print("Rebuilding registration list...")
registration_list.shout()

session.commit()

# Print get all events of type: CustomerRegisteredEvent

evts = ses.get_events_by_type(CustomerRegisteredEvent)
print("---GOT EVNETS %s " % len(evts))
for event in evts:
    print(event)