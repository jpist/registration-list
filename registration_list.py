# from event_store.infrastructure.reflection.reflection import cls_name
# from tests.commands import RegisterCustomerCommand, ChangeCustomerName, UpgradeCustomer
# from tests.customer import Customer
#
# __author__ = 'Andriy Drozdyuk'
#
# from tests.event import CustomerRegisteredEvent, CustomerNameChangedEvent, CustomerBecamePreferred
#
# class CustomerRegistrationService(object):
#     """Application service that manages customer registerations"""
#     def __init__(self, customer_repository):
#         """
#         :param EventStore event_store: event store
#         :return:
#         """
#         self.customer_repository = customer_repository
#
#     def register_customer(self, cmd):
#         self.customer_repository.create(cmd.customer_id, cmd.customer_id)
#
#     def change_name(self, cmd):
#         customer = self.customer_repository.lookup(cmd.customer_id)
#         customer.change_name(cmd.new_name)
#         self.customer_repository.save(customer)
#         #self.persist(CustomerNameChangedEvent(cmd.customer_id, cmd.new_name), self.update_state)
#
#
#     def receive_command(self, cmd):
#         if isinstance(cmd, RegisterCustomerCommand):
#             self.register_customer(cmd)
#
#         elif isinstance(cmd, ChangeCustomerName):
#             self.change_name(cmd)
#
#         elif isinstance(cmd, UpgradeCustomer):
#             if cmd.customer_id in self.customers and not self.customers[cmd.customer_id].preferred:
#                 self.persist(CustomerBecamePreferred(cmd.customer_id), self.update_state)
#
#     def update_state(self, event):
#         if isinstance(event, CustomerRegisteredEvent):
#             self.customers[event.customer_id] = Customer(event.customer_id, event.name)
#         elif isinstance(event, CustomerNameChangedEvent):
#             self.customers[event.customer_id].change_name(event.name)
#         elif isinstance(event, CustomerBecamePreferred):
#             self.customers[event.customer_id].make_preferred()
#
#     def receive_recover(self, event):
#         """Recover state from a given event"""
#         print("Type of event is: %s" % type(event))
#         """Apply event to the domain object"""
#         self.update_state(event)
#         if not hasattr(self, 'seq_number'):
#             self.seq_number = 0
#         self.seq_number += 1
#
#     def number_of_attendees(self):
#         return len(self.customers)
#
#     def shout(self):
#         print("Current attendees are!!!")
#         for att in self.attendee_names():
#             print("  - %s" % att)
#
#     def attendee_names(self):
#         return [customer.name for customer in self.customers.itervalues()]
