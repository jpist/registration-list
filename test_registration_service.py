import uuid
#from event_store.infrastructure.persistence.pickle_serializer import PickleSerializer
#from event_store.infrastructure.persistence.sql_event_store import SqlEventStore
from customer import CustomerRepository
from event_store.infrastructure.persistence.in_memory_event_store import InMemoryEventStore
from event_store.infrastructure.reflection.reflection import obj_name
from faker import Factory
from unittest import TestCase
import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from commands import *
from registration_service import RegistrationService
from test_setup import real_event_store, in_memory_event_store

class RegistrationServiceIntegrationTest(TestCase):
    def setUp(self):
        repository = CustomerRepository(in_memory_event_store())
        self.registration_service = RegistrationService(repository)
        self.fake = Factory.create()
        self.customer_id = 'aaaa-54b0-4e1d-9650-93b229464b9a'
        self.setup_fixtures()
        
    def setup_fixtures(self):
        customer_id = self.customer_id
        self.registration_service.handle(RegisterCustomerCommand(customer_id,
                                                                 'Joe Doe'))

    def test_change_customer_name(self):
        cid = self.customer_id
        cmd = ChangeCustomerNameCommand(cid, self.fake.name())
        self.registration_service.handle(cmd)

    def test_register_vip(self):
        cid = self.customer_id + 'VIP'
        cmd = RegisterVipCustomerCommand(cid, 'Vip Man')
        self.registration_service.handle(cmd)
        

if __name__ == '__main__':
    unittest.main()
